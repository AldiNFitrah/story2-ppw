import java.util.Scanner;

public class Aldi {
    public static void main(String[] args) {
        int a = 1, b = 1;

        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        
        for (int i = 3; i <= n; i++) {
            int tmp = b;
            b = a + b;
            a = tmp;
        }
        System.out.println("Fibo ke-" + n + " adalah " + b);
    }
}

